#include "stdafx.h"
#include "Physics.h"


Physics::Physics()
{
}


Physics::~Physics()
{
}
bool Physics::IsOverlap(GameObject *A, GameObject* B, int type)
{
	switch (type)
	{
	case 0:
		// BBOverlapTest
		return BBOverlapTest(A, B);
		break;
	case 1:
		break;
	}
}
bool Physics::BBOverlapTest(GameObject* A, GameObject* B)
{
	//Object A에 관한 정보들
	float aX, aY, aZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	float aSX, aSY, aSZ;

	//Object B에 관한 정보들
	float bX, bY, bZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	float bSX, bSY, bSZ;

	//calc box A
	A->GetPos(&aX, &aY, &aZ);
	A->GetVol(&aSX, &aSY, &aSZ);
	aMinX = aX - aSX / 2.f;
	aMaxX = aX + aSX / 2.f;

	aMinY = aY - aSY / 2.f;
	aMaxY = aY + aSY / 2.f;

	aMinZ = aZ - aSZ / 2.f;
	aMaxZ = aZ + aSZ / 2.f;

	//calc box B
	B->GetPos(&bX, &bY, &bZ);
	B->GetVol(&bSX, &bSY, &bSZ);
	bMinX = bX - bSX / 2.f;
	bMaxX = bX + bSX / 2.f;

	bMinY = bY - bSY / 2.f;
	bMaxY = bY + bSY / 2.f;

	bMinZ = bZ - bSZ / 2.f;
	bMaxZ = bZ + bSZ / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;
	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;
	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;

}

//완전탄성충돌 주기
void Physics::ProcessCollision(GameObject *A, GameObject* B)
{
	//A
	float aMass, aVX, aVY, aVZ;
	aMass = A->GetMass();
	A->GetVel(&aVX, &aVY, &aVZ);
	//B
	float bMass, bVX, bVY, bVZ;
	bMass = B->GetMass();
	B->GetVel(&bVX, &bVY, &bVZ);

	//final vel
	float afVX, afVY, afVZ;
	float bfVX, bfVY, bfVZ;

	afVX = ((aMass - bMass) / (aMass + bMass)) * aVX + ((2.f* bMass) / (aMass + bMass)) * bVX;
	afVY = ((aMass - bMass) / (aMass + bMass)) * aVY + ((2.f* bMass) / (aMass + bMass)) * bVY;
	afVZ = ((aMass - bMass) / (aMass + bMass)) * aVZ + ((2.f* bMass) / (aMass + bMass)) * bVZ;

	bfVX = ((2.f * aMass) / (aMass + bMass)) * aVX +( (bMass - aMass) / (aMass + bMass) ) * bVX;
	bfVY = ((2.f * aMass) / (aMass + bMass)) * aVY + ((bMass - aMass) / (aMass + bMass)) * bVY;
	bfVZ = ((2.f * aMass) / (aMass + bMass)) * aVZ + ((bMass - aMass) / (aMass + bMass)) * bVZ;

	A->SetVel(afVX, afVY, afVZ);
	B->SetVel(bfVX, bfVY, bfVZ);


}
