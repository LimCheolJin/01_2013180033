/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "ScnMgr.h"
using namespace std;


ScnMgr* g_scnmgr = NULL;
int g_PrevTime = 0;

void RenderScene(int temp)//의미없는 temp값을 넣어주었음.
{
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	int eTime = currentTime - g_PrevTime;
	g_PrevTime = currentTime;

	g_scnmgr->Update(eTime / 1000.f);
	g_scnmgr->RenderScene();

	glutSwapBuffers();

	glutTimerFunc(8, RenderScene, 0); //재귀호출과 비슷하지만 다름.
}

void Display(void)
{

}
void Idle(void)
{
	
}

void MouseInput(int button, int state, int x, int y)
{
	
}

void KeyDownInput(unsigned char key, int x, int y)
{
	g_scnmgr->KeyDownInput(key, x, y);
}
void KeyUpInput(unsigned char key, int x, int y)
{
	g_scnmgr->KeyUpInput(key, x, y);
}

void SpecialKeyDownInput(int key, int x, int y)
{
	g_scnmgr->SpecialKeyDownInput(key, x, y);
}
void SpecialKeyUpInput(int key, int x, int y)
{
	g_scnmgr->SpecialKeyUpInput(key, x, y);
}
int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(800, 1000);
	glutCreateWindow("Game Software Engineering KPU");

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}

	g_scnmgr = new ScnMgr();

	glutDisplayFunc(Display); // 60초에한번불리는게아님이것은. 변화가있으면 바꾸어주라.
	glutIdleFunc(Idle); // 이부분떄문에 렌더신이호출되었떤것임.
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_PrevTime = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(16, RenderScene, 0);
	glutMainLoop();

	
	delete g_scnmgr;
	g_scnmgr = NULL;
	
    return 0;
}

