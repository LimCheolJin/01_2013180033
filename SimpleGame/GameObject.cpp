#include "stdafx.h"
#include "GameObject.h"
#include "Globals.h"
#include <cmath>
#include <iostream>

GameObject::GameObject()
{
	InitPhysics();
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;

	m_age = 0.f;
}


GameObject::~GameObject()
{
}

void GameObject::InitPhysics()
{
	//포지션
	m_posX = 0.f, m_posY = 0.f , m_posZ = 0.f;

	//질량
	m_mass = -1.f;

	//속도
	m_velX = 0.f, m_velY = 0.f, m_velZ = 0.f;

	//가속도
	m_accX = 0.f, m_accY = 0.f, m_accZ = 0.f;

	//부피
	m_volX = 0.f, m_volY = 0.f, m_volZ = 0.f;

	//색깔
	m_r = 0.f, m_g = 0.f, m_b = 0.f, m_a = -1.f;

	m_frictCoef = 0.f;

	m_type = 0;
}

void GameObject::GetColor(float *r, float *g, float *b, float* a)
{
	*r = m_r;
	*g = m_g;
	*b = m_b;
	*a = m_a;
}


void GameObject::GetPos(float *x, float *y, float *z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}
void GameObject::GetVel(float *velX, float *velY, float *velZ)
{
	*velX = m_velX;
	*velY = m_velY;
	*velZ = m_velZ;
}
void GameObject::GetAcc(float *accX, float *accY, float *accZ)
{
	*accX = m_accX;
	*accY = m_accY;
	*accZ = m_accZ;
}
void GameObject::GetVol(float *volX, float *volY, float *volZ)
{
	*volX = m_volX;
	*volY = m_volY;
	*volZ = m_volZ;
}
float GameObject::GetMass()
{
	return m_mass;
}

void GameObject::GetType(int *type)
{
	*type = m_type;
}
float GameObject::GetAge()
{
	return m_age;
}

void GameObject::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}
void GameObject::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}
void GameObject::SetVel(float velX, float velY, float velZ)
{
	m_velX = velX;
	m_velY = velY;
	m_velZ = velZ;
}
void GameObject::SetAcc(float accX, float accY, float accZ)
{
	m_accX = accX;
	m_accY = accY;
	m_accZ = accZ;
}
void GameObject::SetVol(float volX, float volY, float volZ)
{
	m_volX = volX;
	m_volY = volY;
	m_volZ = volZ;
}
void GameObject::SetMass(float mass)
{
	m_mass = mass;
}
void GameObject::SetType(int type)
{
	m_type = type;
}
void GameObject::GetTex(int * id)
{
	*id = m_TextId;
}
void GameObject::SetTex(int id)
{
	m_TextId = id;
}
bool GameObject::CanShootBullet()
{
	if (m_remainingBulletCoolTime < 0.00000001f)
		return true;
	else
		return false;
}
void GameObject::ResetShootBulletCoolTime()
{
	m_remainingBulletCoolTime = m_defaultBulletCoolTime;
}
void GameObject::Update(float eTime)
{
	//Reduce bullet cooltime
	m_remainingBulletCoolTime -= eTime;

	//add to age;
	m_age += eTime;

	////////////////apply friction
	//normalize velocity vector(only x y)
	float velSize = sqrtf(m_velX * m_velX + m_velY * m_velY);
	if (velSize > 0.f)
	{
		float vX = m_velX / velSize; // 정규화된 속도벡터
		float vY = m_velY / velSize; // 정규화된 속도벡터

		//calculate friction size(마찰력크기구하기)
		float nForce = GRAVITY * m_mass;
		float frictionSize = nForce * m_frictCoef;
		float fx = -vX * frictionSize;
		float fy = -vY * frictionSize;

		//calculate acc from friction
		float accX = fx / m_mass;
		float accY = fy / m_mass;

		//update velocity
		float newVelX = m_velX + accX * eTime;
		float newVelY = m_velY + accY * eTime;
		//float newVelZ = m_velZ - GRAVITY * eTime;
		m_velZ = m_velZ - GRAVITY * eTime;

		if (newVelX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = newVelX;
		}
		if (newVelY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = newVelY;
		}
	}
	else if (m_posZ > 0.f)
	{
		m_velZ = m_velZ - GRAVITY * eTime;
	}
	/////////////////////////////////////////////////////
	

	m_posX += m_velX * eTime;
	m_posY += m_velY * eTime;
	m_posZ += m_velZ * eTime;

	if (m_posZ < 0.f)
	{
		m_posZ = 0.f;
		m_velZ = 0.f;
	}
}

void GameObject::AddForce(float x, float y, float z, float eTime)
{
	float accX, accY, accZ;
	accX = accY = accZ = 0.f;
	
	accX = x / m_mass;
	accY = y / m_mass;
	accZ = z / m_mass;

	m_velX = m_velX + accX * eTime;
	m_velY = m_velY + accY * eTime;
	m_velZ = m_velZ + accZ * eTime;
}

void GameObject::SetFriectCoef(float frictCoef)
{
	m_frictCoef = frictCoef;
}


void GameObject::GetFriectCoef(float *friectCoef)
{
	*friectCoef = m_frictCoef;
}

void GameObject::SetParentObj(GameObject* obj)
{
	m_parent = obj;
}
GameObject* GameObject::GetParentObj()
{
	return m_parent;
}
bool GameObject::IsAncestor(GameObject* obj)
{
	//TBD
	if (m_parent != NULL)
	{
		if (obj == m_parent)
		{
			return true;
		}
		/*else
		{
			GameObject* temp = obj->GetParentObj();
			if (temp == m_parent)
			{
				return true;
			}
		}*/
	}
	return false;
}

void GameObject::SetHp(float hp)
{
	m_healthPoint = hp;
}
void GameObject::GetHp(float* hp)
{
	*hp = m_healthPoint;
}
void GameObject::Damage(float damage)
{
	m_healthPoint -= damage;
}