#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <random>
using namespace std;

int ScnMgr::enemyCnt = 0;

int g_normalEnemyTexture = -1;
int g_angryEnemyTexture = -1;
int g_hellEnemyTexture = -1;
int g_testBG = -1; //background
int g_testAnim = -1;
int g_bulletTexture = -1;
int g_frontheadtex = -1;
int g_behindheadtex = -1;
int g_leftheadtex = -1;
int g_rightheadtex = -1;
int g_leftAnimtex = -1;
int g_rightAnimtex = -1;
int g_gameovertex = -1;
int g_cleartex = -1;
int g_starttex = -1;

//sound
int g_testSoundBG = -1;
int g_testSoundFire = -1;
int g_testSoundShot = -1;
int g_jumpSound = -1;
int g_enemyDieSound = -1;
int g_gameoverSound = -1;
//particle
int g_testParticle = -1;
int g_testParticleTex = -1;
int g_fireParticle = -1;
int g_fireParticleTex = -1;

bool b_gameOverSound = true;
default_random_engine dre;
uniform_real_distribution<> uidX(-3.0f, 3.0f);
uniform_real_distribution<> uidY(-4.5f,  6.5f);
uniform_int_distribution<> uid(0, 1);

ScnMgr::ScnMgr()
{
	m_Renderer = new Renderer(800,1000);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	gstate = startscene;

	//Initalize Physics
	m_Physics = new Physics;

	//InitSound
	m_Sound = new Sound();

	for (int i = 0; i < MAX_OBJECT; ++i)
	{
		m_GameObjects[i] = NULL;
	}


	//Add Hero Object
	
	textures_init();
	sounds_init();

	m_GameObjects[HERO_ID] = new GameObject();


	player_init();
	
	

	
}
void ScnMgr::sounds_init()
{
	g_testSoundBG = m_Sound->CreateBGSound("./Sounds/bgm.mp3");
	m_Sound->PlayBGSound(g_testSoundBG, true, 1.f);
	g_testSoundShot = m_Sound->CreateShortSound("./Sounds/shot.mp3");
	g_testSoundFire = m_Sound->CreateShortSound("./Sounds/fire.mp3");
	g_jumpSound = m_Sound->CreateShortSound("./Sounds/jump.mp3");
	g_enemyDieSound = m_Sound->CreateShortSound("./Sounds/enemyDie.mp3");
	g_gameoverSound = m_Sound->CreateShortSound("./Sounds/gameover.mp3");
}
void ScnMgr::textures_init()
{
	g_testAnim = m_Renderer->GenPngTexture("./Textures/12.png");
	g_frontheadtex = m_Renderer->GenPngTexture("./Textures/11.png");
	g_behindheadtex = m_Renderer->GenPngTexture("./Textures/21.png");
	g_leftheadtex = m_Renderer->GenPngTexture("./Textures/31.png");
	g_leftAnimtex = m_Renderer->GenPngTexture("./Textures/32.png");
	g_rightheadtex = m_Renderer->GenPngTexture("./Textures/41.png");
	g_rightAnimtex = m_Renderer->GenPngTexture("./Textures/42.png");
	g_bulletTexture = m_Renderer->GenPngTexture("./Textures/bullet.png");
	g_testBG = m_Renderer->GenPngTexture("./Textures/backgroundt.png");
	g_gameovertex = m_Renderer->GenPngTexture("./Textures/gameover.png");
	g_testParticleTex = m_Renderer->GenPngTexture("./Textures/testParticle.png");
	g_fireParticleTex = m_Renderer->GenPngTexture("./Textures/fire.png");
	g_testParticle = m_Renderer->CreateParticleObject(50, 0, 0, 0, 0, 5, 5, 7, 7, -10, -10, 10, 10);
	g_fireParticle = m_Renderer->CreateParticleObject(50, 0, 0, 0, 0, 5, 5, 7, 7, 0, 20, 0, 20);
	g_cleartex = m_Renderer->GenPngTexture("./Textures/clear.PNG");
	g_starttex = m_Renderer->GenPngTexture("./Textures/start.PNG");
}
void ScnMgr::player_init()
{
	m_GameObjects[HERO_ID]->SetColor(1, 1, 1, 1);
	m_GameObjects[HERO_ID]->SetPos(0, -5, 0);
	m_GameObjects[HERO_ID]->SetVol(0.5f, 0.5f, 0.5f);
	m_GameObjects[HERO_ID]->SetVel(0, 0, 0);
	m_GameObjects[HERO_ID]->SetMass(1);
	m_GameObjects[HERO_ID]->SetFriectCoef(0.7f);
	m_GameObjects[HERO_ID]->SetTex(g_testAnim);
	//m_GameObjects[HERO_ID]->SetTex(g_testTexture);
	m_GameObjects[HERO_ID]->SetHp(100);
	m_GameObjects[HERO_ID]->SetType(HERO_ID);
	m_GameObjects[HERO_ID]->damage = 25.0f;
	m_GameObjects[HERO_ID]->m_texHead = g_behindheadtex;
}
void ScnMgr::stage1_init()
{
	int temp;
	g_normalEnemyTexture = m_Renderer->GenPngTexture("./Textures/defaultEnemyt.PNG");

	for (int i = 0; i < MAX_STAGE1CNT; ++i)
	{
		float ey = uidY(dre);
		float ex = uidX(dre);
		temp = AddObject(ex, ey + 11.0f, 0, 0.5f, 0.5f, 0.5f, 1, 1, 1, 1, 0, 0, 0, 1, TYPE_DEFAULT_ENEMY, 0.7, 100);
		m_GameObjects[temp]->SetTex(g_normalEnemyTexture);
		m_GameObjects[temp]->damage = 2.0f;
		m_GameObjects[temp]->b_particle = false;
	}
}
void ScnMgr::stage2_init()
{
	int temp;
	g_angryEnemyTexture = m_Renderer->GenPngTexture("./Textures/101.png");
	for (int i = 0; i < MAX_STAGE2CNT; ++i)
	{
		float ey = uidY(dre);
		float ex = uidX(dre);
		temp = AddObject(ex, ey + 11.0f, 0, 0.5f, 0.5f, 0.5f, 1, 1, 1, 1, 0, 0, 0, 1, TYPE_ANGRY_ENEMY, 0.7, 100);
		m_GameObjects[temp]->SetTex(g_angryEnemyTexture);
		m_GameObjects[temp]->damage = 4.0f;
		m_GameObjects[temp]->b_particle = false;
	}
}
void ScnMgr::stage3_init()
{
	int temp;
	g_hellEnemyTexture = m_Renderer->GenPngTexture("./Textures/102.png");
	for (int i = 0; i < MAX_STAGE3CNT; ++i)
	{
		float ey = uidY(dre);
		float ex = uidX(dre);
		temp = AddObject(ex, ey + 11.0f, 0, 0.5f, 0.5f, 0.5f, 1, 1, 1, 1, 0, 0, 0, 1, TYPE_HELL_ENEMY, 0.7, 100);
		m_GameObjects[temp]->SetTex(g_hellEnemyTexture);
		m_GameObjects[temp]->damage = 10.0f;
		m_GameObjects[temp]->b_particle = false;
	}
}

//overlap test
// collision processing
ScnMgr::~ScnMgr()
{
	delete m_Renderer;
	m_Renderer = NULL;
}

void ScnMgr::Update(float eTime)
{
	
	float fx, fy, fz;
	float fAmount = 10.f;
	fx = fy = fz = 0.f;

	if (gstate == startscene)
	{
		if (m_keyStart)
		{
			enemyCnt = 0;
			gstate = stage1scene;
			stage1_init();
			player_init();
			b_gameOverSound = true;
			m_keyStart = false;
		}
	}
	if (gstate == stage1scene || gstate == stage2scene || gstate == stage3scene)
	{
		
		if (m_keyW)
		{
			m_GameObjects[HERO_ID]->m_texHead = g_behindheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_testAnim);
			fy += 1.f;
		}
		if (m_keyS)
		{
			m_GameObjects[HERO_ID]->m_texHead = g_frontheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_testAnim);
			fy -= 1.f;
		}
		if (m_keyA)
		{
			m_GameObjects[HERO_ID]->m_texHead = g_rightheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_rightAnimtex);
			fx -= 1.f;
		}
		if (m_keyD)
		{
			m_GameObjects[HERO_ID]->m_texHead = g_leftheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_leftAnimtex);
			fx += 1.f;
		}
		if (m_KeySP)
		{
			fz += 1.f;
		}

		float fSize = sqrtf(fx*fx + fy * fy);
		if (fSize > FLT_EPSILON) // 0으로나누는거 방지
		{
			fx /= fSize;
			fy /= fSize;

			fx *= fAmount;
			fy *= fAmount;
			
			if (m_GameObjects[HERO_ID] != NULL)
				m_GameObjects[HERO_ID]->AddForce(fx, fy, 0, eTime);
		}

		if (fz > FLT_EPSILON)
		{
			float hx, hy, hz;
			m_GameObjects[HERO_ID]->GetPos(&hx, &hy, &hz);
			if (hz < FLT_EPSILON)
			{
				fz *= fAmount * 30.f;
				m_GameObjects[HERO_ID]->AddForce(0, 0, fz, eTime);
			}
		}
		//enemy


		//bullet
		float fxBullet, fyBullet, fzBullet;
		float fAmountBullet = 10.f; //힘의양
		fxBullet = fyBullet = fzBullet = 0.f;

		if (m_keyUp)
		{
			fyBullet += 1.f;
			m_GameObjects[HERO_ID]->m_texHead = g_behindheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_testAnim);
		}
		if (m_keyDown)
		{
			fyBullet -= 1.f;
			m_GameObjects[HERO_ID]->m_texHead = g_frontheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_testAnim);
		}
		if (m_keyLeft)
		{
			fxBullet -= 1.f;
			m_GameObjects[HERO_ID]->m_texHead = g_rightheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_rightAnimtex);
		}
		if (m_keyRight)
		{
			fxBullet += 1.f;
			m_GameObjects[HERO_ID]->m_texHead = g_leftheadtex;
			m_GameObjects[HERO_ID]->SetTex(g_leftAnimtex);
		}
		float fbSize = sqrtf(fxBullet*fxBullet + fyBullet * fyBullet + fzBullet * fzBullet);

		if (fbSize > 0.000000001f)
		{
			fxBullet /= fbSize;
			fyBullet /= fbSize;
			fzBullet /= fbSize;
			fxBullet *= fAmountBullet;
			fyBullet *= fAmountBullet;
			fzBullet *= fAmountBullet;
			//add object for firing bullet
			float hx, hy, hz;
			m_GameObjects[HERO_ID]->GetVel(&hx, &hy, &hz);
			hx = hx + fxBullet;
			hy = hy + fyBullet;
			hz = 0.f;
			float x, y, z;
			m_GameObjects[HERO_ID]->GetPos(&x, &y, &z);
			float s = 0.2f;
			float mass = 1.f;
			float fricCoef = 0.8f;
			int type = TYPE_BULLET;

			//check whether this obj can shoot bullet or not
			if (m_GameObjects[HERO_ID]->CanShootBullet())
			{
				int temp = AddObject(x, y, z,
					s, s, s,
					1, 1, 1, 1,
					hx, hy, hz,
					mass,
					type,
					fricCoef, 1);
				m_GameObjects[temp]->SetParentObj(m_GameObjects[HERO_ID]);
				m_GameObjects[temp]->SetTex(g_bulletTexture);
				m_GameObjects[HERO_ID]->ResetShootBulletCoolTime();
				m_Sound->PlayShortSound(g_testSoundShot, false, 0.1f);
			}
		}

		//stage1
		if (gstate == stage1scene)
		{
			for (int src = 0; src < MAX_OBJECT; src++)
			{
				for (int trg = src + 1; trg < MAX_OBJECT; trg++)
				{
					if (m_GameObjects[src] != NULL && m_GameObjects[trg] != NULL)
					{

						if (m_Physics->IsOverlap(m_GameObjects[src], m_GameObjects[trg]))
						{
							int srctype;
							int trgtype;
							m_GameObjects[src]->GetType(&srctype);
							m_GameObjects[trg]->GetType(&trgtype);

							if (srctype == HERO_ID && trgtype == TYPE_DEFAULT_ENEMY)
							{
								float hp;
								m_GameObjects[src]->GetHp(&hp);
								hp -= m_GameObjects[trg]->damage;
								m_GameObjects[src]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[trg]->damage = 0.0f;
							}
							else if (srctype == TYPE_DEFAULT_ENEMY && trgtype == HERO_ID)
							{
								float hp;
								m_GameObjects[trg]->GetHp(&hp);
								hp -= m_GameObjects[src]->damage;
								m_GameObjects[trg]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->damage = 0.0f;
							}
							else if (srctype == TYPE_DEFAULT_ENEMY && trgtype == TYPE_BULLET)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[src]->b_particle = true;

							}
							else if (srctype == TYPE_BULLET && trgtype == TYPE_DEFAULT_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);

								m_GameObjects[trg]->Damage(10.0f);
								m_GameObjects[trg]->b_particle = true;

							}
							else if (srctype == TYPE_DEFAULT_ENEMY && trgtype == TYPE_DEFAULT_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[trg]->Damage(10.0f);
							}

							else if (srctype == TYPE_BULLET && trgtype == TYPE_BULLET)
							{
								if (!m_GameObjects[src]->IsAncestor(m_GameObjects[trg]) && !m_GameObjects[trg]->IsAncestor(m_GameObjects[src]))
								{
									m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								}
							}
						}
					}
				}
			}

			for (int i = 0; i < MAX_OBJECT; ++i)
			{
				if (m_GameObjects[i] != NULL)
				{
					int type;
					m_GameObjects[i]->GetType(&type);

					if (type == TYPE_DEFAULT_ENEMY)
					{
						float x, y, z;
						m_GameObjects[i]->GetPos(&x, &y, &z);
						y -= 0.05f;
						m_GameObjects[i]->SetPos(x, y, z);


						if (y < -5.5f)
						{
							m_GameObjects[i]->SetPos(uidX(dre), uidY(dre) + 11.0f, z);
							m_GameObjects[i]->damage = 2.0f;
						}
					}
				}
			}

			if (enemyCnt >= MAX_STAGE1CNT)
			{
				gstate = stage2scene;
				enemyCnt = 0;

				for (int i = 0; i < MAX_OBJECT; ++i)
				{
					if (m_GameObjects[i] != NULL)
					{
						if (i != HERO_ID)
						{
							delete m_GameObjects[i];
							m_GameObjects[i] = NULL;
						}
					}
				}
				stage2_init();
			}
		}


		if (gstate == stage2scene)
		{
			for (int src = 0; src < MAX_OBJECT; src++)
			{
				for (int trg = src + 1; trg < MAX_OBJECT; trg++)
				{
					if (m_GameObjects[src] != NULL && m_GameObjects[trg] != NULL)
					{

						if (m_Physics->IsOverlap(m_GameObjects[src], m_GameObjects[trg]))
						{
							int srctype;
							int trgtype;
							m_GameObjects[src]->GetType(&srctype);
							m_GameObjects[trg]->GetType(&trgtype);


							if (srctype == HERO_ID && trgtype == TYPE_ANGRY_ENEMY)
							{
								float hp;
								m_GameObjects[src]->GetHp(&hp);
								hp -= m_GameObjects[trg]->damage;
								m_GameObjects[src]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[trg]->damage = 0.0f;
							}
							else if (srctype == TYPE_ANGRY_ENEMY && trgtype == HERO_ID)
							{
								float hp;
								m_GameObjects[trg]->GetHp(&hp);
								hp -= m_GameObjects[src]->damage;
								m_GameObjects[trg]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->damage = 0.0f;
							}
							else if (srctype == TYPE_ANGRY_ENEMY && trgtype == TYPE_BULLET)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[src]->b_particle = true;

							}
							else if (srctype == TYPE_BULLET && trgtype == TYPE_ANGRY_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[trg]->Damage(10.0f);
								m_GameObjects[trg]->b_particle = true;


							}
							else if (srctype == TYPE_ANGRY_ENEMY && trgtype == TYPE_ANGRY_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[trg]->Damage(10.0f);
							}

							else if (srctype == TYPE_BULLET && trgtype == TYPE_BULLET)
							{
								if (!m_GameObjects[src]->IsAncestor(m_GameObjects[trg]) && !m_GameObjects[trg]->IsAncestor(m_GameObjects[src]))
								{
									m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								}
							}


						}
					}
				}
			}
			for (int i = 0; i < MAX_OBJECT; ++i)
			{
				if (m_GameObjects[i] != NULL)
				{
					int type;
					m_GameObjects[i]->GetType(&type);

					if (type == TYPE_ANGRY_ENEMY)
					{
						float x, y, z;
						m_GameObjects[i]->GetPos(&x, &y, &z);
						y -= 0.075f;
						m_GameObjects[i]->SetPos(x, y, z);

						if (y < -5.5f)
						{
							m_GameObjects[i]->SetPos(uidX(dre), uidY(dre) + 11.0f, z);
							m_GameObjects[i]->damage = 4.0f;
						}
					}
				}
			}

			if (enemyCnt >= MAX_STAGE2CNT)
			{
				gstate = stage3scene;
				enemyCnt = 0;

				for (int i = 0; i < MAX_OBJECT; ++i)
				{
					if (m_GameObjects[i] != NULL)
					{
						if (i != HERO_ID)
						{
							delete m_GameObjects[i];
							m_GameObjects[i] = NULL;
						}
					}
				}
				stage3_init();
			}
		}

		if (gstate == stage3scene)
		{
			for (int src = 0; src < MAX_OBJECT; src++)
			{
				for (int trg = src + 1; trg < MAX_OBJECT; trg++)
				{
					if (m_GameObjects[src] != NULL && m_GameObjects[trg] != NULL)
					{

						if (m_Physics->IsOverlap(m_GameObjects[src], m_GameObjects[trg]))
						{
							int srctype;
							int trgtype;
							m_GameObjects[src]->GetType(&srctype);
							m_GameObjects[trg]->GetType(&trgtype);


							if (srctype == HERO_ID && trgtype == TYPE_HELL_ENEMY)
							{
								float hp;
								m_GameObjects[src]->GetHp(&hp);
								hp -= m_GameObjects[trg]->damage;
								m_GameObjects[src]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[trg]->damage = 0.0f;
							}
							else if (srctype == TYPE_HELL_ENEMY && trgtype == HERO_ID)
							{
								float hp;
								m_GameObjects[trg]->GetHp(&hp);
								hp -= m_GameObjects[src]->damage;
								m_GameObjects[trg]->SetHp(hp);
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->damage = 0.0f;
							}
							else if (srctype == TYPE_HELL_ENEMY && trgtype == TYPE_BULLET)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[src]->b_particle = true;

							}
							else if (srctype == TYPE_BULLET && trgtype == TYPE_HELL_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[trg]->Damage(10.0f);
								m_GameObjects[trg]->b_particle = true;


							}
							else if (srctype == TYPE_HELL_ENEMY && trgtype == TYPE_HELL_ENEMY)
							{
								m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								m_GameObjects[src]->Damage(10.0f);
								m_GameObjects[trg]->Damage(10.0f);
							}

							else if (srctype == TYPE_BULLET && trgtype == TYPE_BULLET)
							{
								if (!m_GameObjects[src]->IsAncestor(m_GameObjects[trg]) && !m_GameObjects[trg]->IsAncestor(m_GameObjects[src]))
								{
									m_Physics->ProcessCollision(m_GameObjects[src], m_GameObjects[trg]);
								}
							}


						}
					}
				}
			}
			for (int i = 0; i < MAX_OBJECT; ++i)
			{
				if (m_GameObjects[i] != NULL)
				{
					int type;
					m_GameObjects[i]->GetType(&type);

					if (type == TYPE_HELL_ENEMY)
					{
						float x, y, z;
						m_GameObjects[i]->GetPos(&x, &y, &z);
						y -= 0.11f;
						m_GameObjects[i]->SetPos(x, y, z);


						if (y < -5.5f)
						{
							m_GameObjects[i]->SetPos(uidX(dre), uidY(dre) + 11.0f, z);
							m_GameObjects[i]->damage = 10.0f;
						}
					}
				}
			}

			if (enemyCnt >= MAX_STAGE3CNT)
			{
				gstate = clearscene;
				enemyCnt = 0;

				for (int i = 0; i < MAX_OBJECT; ++i)
				{
					if (m_GameObjects[i] != NULL)
					{
						if (i != HERO_ID)
						{
							delete m_GameObjects[i];
							m_GameObjects[i] = NULL;
						}
					}
				}
			}
		}
		for (int i = 0; i < MAX_OBJECT; ++i)
		{
			if (m_GameObjects[i] != NULL)
			{
				m_GameObjects[i]->Update(eTime);
			}

		}

		//백그라운드와의 충돌처리를 위한것.
		float px, py, pz;
		m_GameObjects[HERO_ID]->GetPos(&px, &py, &pz);
		if (px < -3.0f)
		{
			px = -3.0f;
			m_GameObjects[HERO_ID]->SetPos(px, py, pz);
		}
		if (px > 3.0f)
		{
			px = 3.0f;
			m_GameObjects[HERO_ID]->SetPos(px, py, pz);
		}
		if (py > 4.0f)
		{
			py = 4.0f;
			m_GameObjects[HERO_ID]->SetPos(px, py, pz);
		}
		if (py < -4.5f)
		{
			py = -4.5f;
			m_GameObjects[HERO_ID]->SetPos(px, py, pz);
		}

	}

	DoGarbagecollection();
	
}


void ScnMgr::RenderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	static float pTime = 0.0f;
	pTime += 0.016;
	if (gstate == startscene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_starttex, 1.f);
	}
	if (gstate == stage1scene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_testBG, 1.f);
		// Renderer Test
		for (int i = 0; i < MAX_OBJECT; ++i)
		{
			float x, y, z = 0.f;
			float sx, sy, sz = 0.f;
			float r, g, b, a = 0.f;
			int texID = -1;
			if (m_GameObjects[i] != NULL)
			{
				m_GameObjects[i]->GetPos(&x, &y, &z);
				x = x * 100.f;
				y = y * 100.f;
				z = z * 100.f;
				m_GameObjects[i]->GetVol(&sx, &sy, &sz);
				sx = sx * 100.f;
				sy = sy * 100.f;
				sz = sz * 100.f;
				m_GameObjects[i]->GetColor(&r, &g, &b, &a);
				m_GameObjects[i]->GetTex(&texID);


				int type;
				m_GameObjects[i]->GetType(&type);
				if (i == HERO_ID)
				{
					
					m_Renderer->DrawTextureRect(x, y - 60.0f, z, sx * 4, sy * 4, sz * 4, r, g, b, a, m_GameObjects[HERO_ID]->m_texHead, false);
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 9, 1, pTime * 8, 0);
					m_Renderer->DrawParticle(g_testParticle, x, y+10.0f, z , 3, 1, 1, 1, 1, -1, 1, g_testParticleTex, 0.8f, pTime, 1.f);
				}
				else if (type == TYPE_DEFAULT_ENEMY)
				{			
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 5, 1, pTime * 4, 0);
					
					if (m_GameObjects[i]->b_particle)
					{
						m_Renderer->DrawParticle(g_fireParticle, x, y + 10.0f, z, 3, 1, 1, 1, 1, -1, 1, g_fireParticleTex, 0.3f, pTime, 1.f);
					}
				}
				else
				{
					float vx, vy, vz;
					m_GameObjects[i]->GetVel(&vx, &vy, &vz);
					m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID);
					
				}

				if (type == HERO_ID)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 50, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
				if (type == TYPE_DEFAULT_ENEMY)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 25, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
			}
		}
	}

	if (gstate == stage2scene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_testBG, 1.f);
		for (int i = 0; i < MAX_OBJECT; ++i)
		{
			float x, y, z = 0.f;
			float sx, sy, sz = 0.f;
			float r, g, b, a = 0.f;
			int texID = -1;
			if (m_GameObjects[i] != NULL)
			{
				m_GameObjects[i]->GetPos(&x, &y, &z);
				x = x * 100.f;
				y = y * 100.f;
				z = z * 100.f;
				m_GameObjects[i]->GetVol(&sx, &sy, &sz);
				sx = sx * 100.f;
				sy = sy * 100.f;
				sz = sz * 100.f;
				m_GameObjects[i]->GetColor(&r, &g, &b, &a);
				m_GameObjects[i]->GetTex(&texID);


				int type;
				m_GameObjects[i]->GetType(&type);
				if (i == HERO_ID)
				{
					m_Renderer->DrawTextureRect(x, y - 60.0f, z, sx * 4, sy * 4, sz * 4, r, g, b, a, m_GameObjects[HERO_ID]->m_texHead, false);
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 9, 1, pTime * 8, 0);
					m_Renderer->DrawParticle(g_testParticle, x, y + 10.0f, z, 3, 1, 1, 1, 1, -1, 1, g_testParticleTex, 0.8f, pTime, 1.f);
				}
				else if (type == TYPE_ANGRY_ENEMY)
				{
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 4, 1, pTime * 4, 0);
					
					if (m_GameObjects[i]->b_particle)
					{
						m_Renderer->DrawParticle(g_fireParticle, x, y + 10.0f, z, 3, 1, 1, 1, 1, -1, 1, g_fireParticleTex, 0.3f, pTime, 1.f);
					}
				}
				else
				{
					float vx, vy, vz;
					m_GameObjects[i]->GetVel(&vx, &vy, &vz);
	
					m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID);
					
				}
				if (type == HERO_ID)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 50, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
				if (type == TYPE_ANGRY_ENEMY)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 25, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
			}
		}
	}

	if (gstate == stage3scene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_testBG, 1.f);
		for (int i = 0; i < MAX_OBJECT; ++i)
		{
			float x, y, z = 0.f;
			float sx, sy, sz = 0.f;
			float r, g, b, a = 0.f;
			int texID = -1;
			if (m_GameObjects[i] != NULL)
			{
				m_GameObjects[i]->GetPos(&x, &y, &z);
				x = x * 100.f;
				y = y * 100.f;
				z = z * 100.f;
				m_GameObjects[i]->GetVol(&sx, &sy, &sz);
				sx = sx * 100.f;
				sy = sy * 100.f;
				sz = sz * 100.f;
				m_GameObjects[i]->GetColor(&r, &g, &b, &a);
				m_GameObjects[i]->GetTex(&texID);


				int type;
				m_GameObjects[i]->GetType(&type);
				if (i == HERO_ID)
				{

					m_Renderer->DrawTextureRect(x, y - 60.0f, z, sx * 4, sy * 4, sz * 4, r, g, b, a, m_GameObjects[HERO_ID]->m_texHead, false);
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 9, 1, pTime * 8, 0);
					m_Renderer->DrawParticle(g_testParticle, x, y + 10.0f, z, 3, 1, 1, 1, 1, -1, 1, g_testParticleTex, 0.8f, pTime, 1.f);
				}
				else if (type == TYPE_HELL_ENEMY)
				{
					m_Renderer->DrawTextureRectAnim(x, y, z, sx, sy, sz, r, g, b, a, texID, 4, 1, pTime * 4, 0);
					if (m_GameObjects[i]->b_particle)
					{
						m_Renderer->DrawParticle(g_fireParticle, x, y + 10.0f, z, 3, 1, 1, 1, 1, -1, 1, g_fireParticleTex, 0.3f, pTime, 1.f);
					}
				}
				else
				{
					float vx, vy, vz;
					m_GameObjects[i]->GetVel(&vx, &vy, &vz);					
					m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, a, texID);
					
				}
				if (type == HERO_ID)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 50, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
				if (type == TYPE_HELL_ENEMY)
				{
					float hp;
					m_GameObjects[i]->GetHp(&hp);
					m_Renderer->DrawSolidRectGauge(x, y, z, 0, 25, 0, sx, sy / 8.f, 1, 1, 1, 1, 1, hp);
				}
			}
		}
	}

	if (gstate == clearscene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_cleartex, 1.f);
	}

	if (gstate == gameoverscene)
	{
		m_Renderer->DrawGround(0, 0, 0, 800, 1000, 0, 1, 1, 1, 1, g_gameovertex, 1.0f);
		if (b_gameOverSound)
		{
			
			m_Sound->PlayShortSound(g_gameoverSound, false, 1.f);
			b_gameOverSound = false;
		}
		for (int i = 0; i < MAX_OBJECT; ++i)
		{
			if (m_GameObjects[i] != NULL)
			{
				int type;
				m_GameObjects[i]->GetType(&type);
				if (type != HERO_ID)
				{
					delete m_GameObjects[i];
					m_GameObjects[i] = NULL;
				}
			}
		}
	}
}

int ScnMgr::AddObject(float x, float y, float z, float sx, float sy, float sz, float r, float g, float b, float a, float vx, float vy, float vz, float mass, int type, float fricCoef, float hp)
{
	//search empty slot
	int idx = -1;
	for (int i = 0; i < MAX_OBJECT; ++i)
	{
		if (m_GameObjects[i] == NULL)
		{
			idx = i;
			break;
		}
	}
	if (idx == -1)
	{
		std::cout << "No more remaining object" << std::endl;
		return -1;
	}

	
	m_GameObjects[idx] = new GameObject();
	m_GameObjects[idx]->SetColor(r,g,b, a);
	m_GameObjects[idx]->SetPos(x, y, z);
	m_GameObjects[idx]->SetVol(sx,sy,sz);
	m_GameObjects[idx]->SetVel(vx,vy,vz );
	m_GameObjects[idx]->SetMass(mass);
	m_GameObjects[idx]->SetFriectCoef(fricCoef);
	m_GameObjects[idx]->SetType(type);
	m_GameObjects[idx]->SetHp(hp);
	return idx;

}
void ScnMgr::DeleteObject(int idx)
{
	if (idx < 0)
	{
		std::cout << "Negative idx does not allowed.  " << idx << std::endl;
		return;
	}
	if (idx >= MAX_OBJECT)
	{
		std::cout << "Requested idx exceeds MAX_OBJECTS count.  " << idx <<std::endl;
		return;
	}
	if (m_GameObjects[idx] != NULL)
	{
		int type;
		m_GameObjects[idx]->GetType(&type);
		if (type == TYPE_DEFAULT_ENEMY)
		{
			m_GameObjects[idx]->b_particle = false;
			enemyCnt++;
			m_Sound->PlayShortSound(g_enemyDieSound, false, 0.1f);
		}
		else if (type == TYPE_ANGRY_ENEMY)
		{
			m_GameObjects[idx]->b_particle = false;
			enemyCnt++;
			m_Sound->PlayShortSound(g_enemyDieSound, false, 0.1f);
		}
		else if (type == TYPE_HELL_ENEMY)
		{
			m_GameObjects[idx]->b_particle = false;
			enemyCnt++;
			m_Sound->PlayShortSound(g_enemyDieSound, false, 0.1f);
		}
	
		delete m_GameObjects[idx];
		m_GameObjects[idx] = NULL;
		
	}

}



void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = true;
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = true;
	}
	if (key == 'a' || key == 'A')
	{
		m_keyA = true;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = true;
	}
	if (gstate == stage1scene || gstate == stage2scene || gstate == stage3scene)
	{
		if (key == ' ')
		{
			m_KeySP = true;
			m_Sound->PlayShortSound(g_jumpSound, false, 0.2f);
		}
	}

	if (gstate == startscene)
	{
		if (key == 'R' || key == 'r')
		{
			m_keyStart = true;
		}
	}
	if (gstate == gameoverscene || gstate == clearscene)
	{
		if (key == 'i' || key == 'I')
		{
			gstate = startscene;
		}
	}
}
void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_keyW = false;
	}
	if (key == 's' || key == 'S')
	{
		m_keyS = false;
	}
	if (key == 'a' || key == 'A')
	{
		m_keyA = false;
	}
	if (key == 'd' || key == 'D')
	{
		m_keyD = false;
	}
	if (key == ' ')
	{
		m_KeySP = false;
	}

	if (gstate == startscene)
	{
		if (key == 'R' || key == 'r')
		{
			m_keyStart = false;
		}
	}
	if (gstate == gameoverscene || gstate == clearscene)
	{
		if (key == 'i' || key == 'I')
		{
			m_KeyRestart = false;
		}
	}
}


void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = true;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = true;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = true;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = true;
	}
}
void ScnMgr::SpecialKeyUpInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_keyUp = false;
	}
	if (key == GLUT_KEY_LEFT)
	{
		m_keyLeft = false;
	}
	if (key == GLUT_KEY_RIGHT)
	{
		m_keyRight = false;
	}
	if (key == GLUT_KEY_DOWN)
	{
		m_keyDown = false;
	}
}

void ScnMgr::DoGarbagecollection()
{
	//delete bulletes those zero vel

	for (int i = 0; i < MAX_OBJECT; ++i)
	{
		if (m_GameObjects[i] != NULL)
		{
			
			int type;
			m_GameObjects[i]->GetType(&type);
			if (type == TYPE_BULLET)
			{
				
				float vx, vy, vz;
				m_GameObjects[i]->GetVel(&vx, &vy, &vz);
				float vSize = sqrtf(vx*vx + vy * vy + vz *vz);
				if (vSize < FLT_EPSILON)
				{
					DeleteObject(i);
					continue;
				}
			}
			
			float hp;
			m_GameObjects[i]->GetHp(&hp);
			if (type != HERO_ID)
			{
				
				if (hp < FLT_EPSILON)
				{
					DeleteObject(i);
					continue;
				}
			}
			else
			{
				if (hp < FLT_EPSILON)
				{
					gstate = gameoverscene;
					m_GameObjects[HERO_ID]->SetHp(1);
				}
			}
		}
	}
	
}
