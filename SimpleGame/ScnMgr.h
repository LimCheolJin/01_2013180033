#pragma once
#include "Renderer.h"
#include "GameObject.h"
#include "Globals.h"
#include "Physics.h"
#include "Sound.h"

enum gameState
{
	startscene = 100, stage1scene, stage2scene, stage3scene, clearscene,gameoverscene
};
class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene(void);
	//int AddObject(float x, float y, float z, float sx, float sy, float sz, float r, float g, float b, float a);
	int AddObject(float x, float y, float z, float sx, float sy, float sz, float r, float g, float b, float a, float vx, float vy, float vz, float mass, int type, float fricCoef,float hp);
	void DeleteObject(int idx);
	void Update(float eTime);
	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(unsigned char key, int x, int y);
	void SpecialKeyUpInput(unsigned char key, int x, int y);
	void stage1_init();
	void stage2_init();
	void stage3_init();
	void player_init();
	void textures_init();
	void sounds_init();
	void startSceneLoop();

	gameState gstate;

	static int enemyCnt;
	//overlap
	//bool BBCollision(float minX, float minY, float minZ, float maxX, float maxY, float maxZ, float minX1, float minY1, float minZ1, float maxX1, float maxY1, float maxZ1);
private:
	Renderer *m_Renderer = NULL;
	GameObject* m_GameObjects[MAX_OBJECT];
	Physics* m_Physics = NULL;
	Sound* m_Sound = NULL;
	//keyinputs
	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_KeySP = false;
	//special keyinputs
	bool m_keyUp = false;
	bool m_keyLeft = false;
	bool m_keyDown = false;
	bool m_keyRight = false;
	bool m_keyStart = false;
	bool m_KeyRestart = false;

	//가비지컬렉터
	void DoGarbagecollection();

};
