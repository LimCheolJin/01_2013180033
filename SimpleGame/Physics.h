#pragma once
#include "GameObject.h"
class Physics
{
public:
	Physics();
	~Physics();

	bool IsOverlap(GameObject *A, GameObject* B, int type = 0);
	void ProcessCollision(GameObject *A, GameObject* B);

private:
	bool BBOverlapTest(GameObject* A, GameObject* B);
};

