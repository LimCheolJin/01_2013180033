#pragma once

class GameObject
{
private:
	float m_x, m_y, m_z;
	float m_sx, m_sy, m_sz;
	float m_r, m_g, m_b, m_a;

	//포지션
	float m_posX, m_posY, m_posZ;

	//질량
	float m_mass;

	//속도
	float m_velX, m_velY, m_velZ;

	//가속도
	float m_accX, m_accY, m_accZ;

	//부피
	float m_volX, m_volY, m_volZ;

	//마찰계수
	float m_frictCoef;

	//타입
	int m_type;
	GameObject* m_parent = NULL;

	//불릿의 쿨타임을 위함.
	float m_remainingBulletCoolTime = 0.f;
	float m_defaultBulletCoolTime = 0.2f;

	int m_TextId = -1;

	// hp, damage
	float m_healthPoint;

	//생성되고나서 얼마나시간이흘럿는지
	float m_age;
	
public:
	GameObject();
	~GameObject();

	
	void GetColor(float *r, float *g, float *b, float* a);
	void GetPos(float *x, float *y, float *z);
	void GetVel(float *velX, float *velY, float *velZ);
	void GetAcc(float *accX, float *accY, float *accZ);
	void GetVol(float *volX, float *volY, float *volZ);
	void GetType(int *type);
	float GetMass();
	void GetFriectCoef(float* friectCoef);
	void GetTex(int * id);
	float GetAge();
	
	void SetColor(float r, float g, float b, float a);
	void SetPos(float x, float y, float z);
	void SetVel(float velX, float velY, float velZ);
	void SetAcc(float accX, float accY, float accZ);
	void SetVol(float volX, float volY, float volZ);
	void SetMass(float mass);
	void SetFriectCoef(float frictCoef);
	void SetType(int type);
	void SetTex(int id);

	void InitPhysics();
	void Update(float eTime);

	void AddForce(float x, float y, float z, float eTime);

	bool CanShootBullet();
	void ResetShootBulletCoolTime();


	void SetParentObj(GameObject* obj);
	GameObject* GetParentObj();
	bool IsAncestor(GameObject* obj);

	void SetHp(float hp);
	void GetHp(float* hp);
	void Damage(float damage);

	float damage;

	int m_texHead = -1;

	bool b_particle = false;
};

